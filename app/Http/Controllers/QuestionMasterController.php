<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\QuestionMaster;
use App\Model\OptionMaster;
use App\Model\QuestionCategory;
use App\Model\QuestionType;

use Yajra\DataTables\DataTables;



class QuestionMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $questions = QuestionMaster::all();

            return Datatables::of($questions)
                ->addColumn('actions', function ($question) {

                    $output = '';

                    $output .= '<a href="'.url('/admin/question-master', ['id' => $question->id]).'/edit"> <i class="fa fa-pencil btn" title="Show" style="color:black;"></i> </a>';
                    

                    return $output;
                })
                ->addColumn('id', function ($question) {
                    return $question->id;
                })->addColumn('question', function ($question) {
                    return $question->question;
                })->addColumn('category', function ($question) {
                    return $question->category->name;
                })->addColumn('type', function ($question) {
                    return ucfirst($question->type->name);
                })->addColumn('difficulty', function ($question) {
                    return ucfirst($question->difficulty);
                
                })->rawColumns(['actions','diff'])->make(true);
        }

        return view('question.index', [ 'questions' => QuestionMaster::all() ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://opentdb.com/api.php?amount=10');

        $response->getStatusCode(); 
        $response->getHeaderLine('content-type'); 
        $data = json_decode($response->getBody())->results;
        // dd($data);
        \DB::beginTransaction();
        foreach ($data as $key => $question) {
            // dd($question->category);
            if(!QuestionCategory::where('name',$question->category)->exists())
            {
                $category = new QuestionCategory();
                $category->name = $question->category;
                $category->save();
            }
            else
            {
                $category = QuestionCategory::where('name',$question->category)->first();
            }
            
            if(!QuestionType::where('name',$question->type)->exists())
            {
                $type = new QuestionType();
                $type->name = $question->type;
                $type->save();
            }
            else
            {
                $type = QuestionType::where('name',$question->type)->first();
            }

            $questionInfo = [];
            $questionInfo['question'] = $question->question;
            $questionInfo['category_id'] = $category->id;
            $questionInfo['question_type_id'] = $type->id;
            $questionInfo['difficulty'] = $question->difficulty;

            $insertedQuestion = QuestionMaster::create($questionInfo);


            $optionInfo = [];
            $optionInfo['question_id'] = $insertedQuestion->id;
            $optionInfo['option'] = $question->correct_answer;
            $optionInfo['is_correct'] = 1;
            
            OptionMaster::create($optionInfo);

            // dd($insertedQuestion);

            foreach ($question->incorrect_answers as $key => $incorrect_answers) {
                $optionInfo = [];
                $optionInfo['question_id'] = $insertedQuestion->id;
                $optionInfo['option'] = $incorrect_answers;
                $optionInfo['is_correct'] = 0;
                
                OptionMaster::create($optionInfo);
            }
        }
        \DB::commit();
        return redirect('admin/question-master');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = QuestionMaster::find($id);
        $categories = QuestionCategory::pluck('name','id')->all();
        $types = QuestionType::pluck('name','id')->all();
        $difficulty = ['easy','medium','hard'];
        return view('question.edit',compact('question','categories','types','difficulty'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        \DB::beginTransaction();

            $question = QuestionMaster::find($id);
            $data = $request->all();
            QuestionMaster::find($id)->update($data);

            OptionMaster::where('question_id',$id)->delete();
            foreach ($request->option as $key => $option) {
                $optionInfo = [];
                $optionInfo['question_id'] = $id;
                $optionInfo['option'] = $option;
                if($request->correct_answer == $key)
                    $optionInfo['is_correct'] = 1;
                else
                    $optionInfo['is_correct'] = 0;
                
                OptionMaster::create($optionInfo);
            }


        \DB::commit();
        return redirect('admin/question-master');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
