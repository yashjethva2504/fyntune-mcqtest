<?php

namespace App\Http\Controllers;

use App\Model\Test;
use App\Model\QuestionMaster;
use App\Model\OptionMaster;
use App\Model\QuestionCategory;
use App\Model\QuestionType;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TestController extends Controller
{
    public function index()
    {
    	return view('test_main');
    }

    public function login(Request $request)
    {
    	$request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email',
        ]);
    	if(Test::where('email',$request->email)->exists())
    	{
    		$test = Test::where('email',$request->email)->first();
    		return view('test.result',compact('test'));
    	}
    	else
    	{
    		$student = $request->all();
    		// dd($student);
    		$questions = QuestionMaster::all();
    		$options = OptionMaster::all();
    		foreach ($questions as $key => $question) {
    			$question['options'] = $question->options;
    			$question['category'] = $question->category->name;
    			$question['type'] = $question->type->name;
    		}

    		return view('test.test',compact('student','questions','options'));
    	}
    }
    public function store(Request $request){

    	$test = new Test();
    	$test->name = $request->student_name;
    	$test->email = $request->student_email;
    	$test->total_questions = $request->total_questions;
    	$test->right_answers = $request->right_answers;
    	$test->wrong_answers = $request->wrong_answers;
    	$test->save();
    	return $test->id;
    }


    public function result($id)
    {
    	$test = Test::find($id);
    	return view('test.result',compact('test'));
    	dd($test);
    }

    public function results(Request $request)
    {
    	if($request->ajax()){
            $results = Test::all();

            return Datatables::of($results)
                ->addColumn('actions', function ($result) {

                    $output = '';

                    $output .= '<a href="'.url('/admin/user-result', ['id' => $result->id]).'"> <i class="fa fa-eye btn" title="Show" style="color:black;"></i> </a>';
                    

                    return $output;
                })->addColumn('id', function ($result) {
                    return $result->id;
                })->addColumn('name', function ($result) {
                    return $result->name;
                })->addColumn('email', function ($result) {
                    return $result->email;
                })->addColumn('total_questions', function ($result) {
                    return $result->total_questions;
                })->addColumn('right_answers', function ($result) {
                    return $result->right_answers;
                
                })->rawColumns(['actions','diff'])->make(true);
        }

        return view('result.index', [ 'results' => Test::all() ] );
    }

    public function userResults($id)
    {
        $test = Test::find($id);
        return view('result.user_result', compact('test'));

    }
}
