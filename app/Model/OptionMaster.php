<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OptionMaster extends Model
{
    protected $fillable = ['question_id','option','is_correct'];
}
