<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuestionMaster extends Model
{
    protected $fillable = ['question','category_id','question_type_id','difficulty'];

    public function category()
    {
    	return $this->belongsTo('App\Model\QuestionCategory', 'category_id', 'id');	
    }

    public function type()
    {
    	return $this->belongsTo('App\Model\QuestionType', 'question_type_id', 'id');	
    }

    public function options(){
    	return $this->hasMany('\App\Model\OptionMaster', 'question_id');
    }
}
