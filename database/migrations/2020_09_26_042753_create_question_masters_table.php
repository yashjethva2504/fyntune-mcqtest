<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_masters', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->text('question');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('question_type_id');
            $table->string('difficulty');

            $table->foreign('category_id')
                ->references('id')->on('question_categories');

            $table->foreign('question_type_id')
                ->references('id')->on('question_types');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_masters');
    }
}
