<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptionMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('option_masters', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('question_id');
            $table->text('option');
            $table->boolean('is_correct');

            $table->foreign('question_id')
                ->references('id')->on('question_masters');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('option_masters');
    }
}
