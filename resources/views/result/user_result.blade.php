@extends('layouts.app')

@section('title') User Result @endsection
@section('subtitle') User Result @endsection

@section('page-level-css')
    <link href="{{ URL::asset('template/assets/global/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-notebook font-dark"></i>
                        <span class="caption-subject bold uppercase"> User Result</span>
                    </div>
                </div>
                <div class="portlet-body form">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <p style="margin:0px"><b><h4>Name: </b> {{$test->name}} </h4></p>
                                <p style="margin:0px"><b><h4>Email: </b> {{$test->email}} </h4></p>
                                <p style="margin:0px"><b><h4>Total Questions: </b> {{$test->total_questions}} </h4></p>
                                <p style="margin:0px"><b><h4>Correct Answers: </b> {{$test->right_answers}} </h4></p>
                                <p style="margin:0px"><b><h4>Incorrect Answers: </b> {{$test->wrong_answers}} </h4></p>
                                <p style="margin:0px"><b><h4>Result: </b> {{$test->right_answers}} / {{$test->total_questions}} </h4></p>
                            </b>
                        </div>
                    </div>
                <center><a href="{{URL('/admin/result')}}"><button type="submit" class="btn green submit ">Go Back</button></a></center>
                    

                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>

    </div>

@endsection
<?php
    $js_data = array();
?>
@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
@endsection

@section('page-level-scripts-js')
    <script src="{{ URL::asset('template/assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/pages/scripts/components-bootstrap-select.min.js') }}" type="text/javascript"></script>
@endsection