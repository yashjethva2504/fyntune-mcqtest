@extends('layouts.app')

@section('title') Result @endsection
@section('subtitle') Result @endsection

@section('page-level-css')
    <link href="{{ URL::asset('template/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-rocket font-dark"></i>
                        <span class="caption-subject bold uppercase"> Result</span>
                    </div>
                </div>

                <div class="portlet-body table-both-scroll">
                    
                    <table class="table table-striped table-bordered table-hover order-column" id="result">
                        <thead>
                        <tr>
                            <th> Action </th>
                            <th> ID </th>
                            <th> Name </th>
                            <th> Email </th>
                            <th> Total Questions </th>
                            <th> # of Correct Answers </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>


            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
@endsection

@section('page-level-scripts-js')

    @if(Session::has('status'))
        <script>alert('{{Session::get('status')}}');</script>
    @endif
    <script>
        function deleteConfirm() {
            return confirm("Do you want to delete this record?");
        }
    </script>

    <script>
        $( document ).ready(function() {

            $('#result').dataTable({
                processing: true,
                serverSide: true,

                ajax: {

                    url: '{{ url('admin/result') }}',
                    method: 'GET'
                },
                columns: [
                    {data: 'actions', 'searchable':false, orderable:false},
                    {data: 'id'},
                    {data: 'name'},
                    {data: 'email'},
                    {data: 'total_questions'},
                    {data: 'right_answers'},

                ],

                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 15, 20, -1],
                    [10, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "pageLength": 10,

                "order": [], // set first column as a default sort by asc

                "pagingType": "bootstrap_full_number",

                "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",                // horizobtal scrollable datatable
                buttons: [
                    { extend: 'excel', className: 'btn green btn-outline ' },
                    { extend: 'csv', className: 'btn purple btn-outline ' },
                    { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'}
                ],
            });

        });
    </script>

@endsection
