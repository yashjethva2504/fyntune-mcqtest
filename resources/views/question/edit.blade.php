@extends('layouts.app')

@section('title') Edit Question @endsection
@section('subtitle') Edit Question @endsection

@section('page-level-css')
    <link href="{{ URL::asset('template/assets/global/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-notebook font-dark"></i>
                        <span class="caption-subject bold uppercase"> Edit Question</span>
                    </div>
                </div>
                <div class="portlet-body form">

                    @if($errors != NULL)
                        <div class="row">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li><h5 style="color: red">{{$error}}</h5></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{route('question-master.update', $question->id )}}" id="questionform" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-body">
                            <div class="row">
                                
                                <div class="form-group col-md-12">
                                    <label for="name">Question <span class="required" aria-required="true">*</span></label>
                                    
                                    <textarea class="form-control " id="question" name="question">{{$question->question}}</textarea>
                                        
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="name">Category <span class="required" aria-required="true">*</span></label>
                                    <select name="category_id" id="category_id" class="form-control selectpicker" required>
                                        @foreach($categories as $key => $category)
                                        <option value="{{$key}}"
                                            @if( $question->category_id == $key )
                                                selected
                                            @endif
                                        >{{$category}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="name">Question Type <span class="required" aria-required="true">*</span></label>
                                    <select name="question_type_id" id="question_type_id" class="form-control selectpicker" required>
                                        @foreach($types as $key => $type)
                                        <option value="{{$key}}"
                                            @if( $question->question_type_id == $key )
                                                selected
                                            @endif
                                        >{{ucfirst($type)}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="name">Difficulty Level <span class="required" aria-required="true">*</span></label>
                                    <select name="difficulty" id="difficulty" class="form-control selectpicker" required>
                                        @foreach($difficulty as $key => $difficulty_level)
                                        <option value="{{$difficulty_level}}"
                                            @if( $question->difficulty == $difficulty_level )
                                                selected
                                            @endif
                                        >{{ucfirst($difficulty_level)}}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                            
                            
                        </div>
                        @foreach($question->options as $key => $option)
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="name">Option {{$key + 1}} <span class="required" aria-required="true">*</span></label>
                                    <label for="male" class="pull-right"> Correct Answer </label> <input class="pull-right" type="radio" id="optionRadio{{$key}}" name="correct_answer" value="{{$key}}" required 
                                        @if($option->is_correct == 1)
                                            checked
                                        @endif >
                                    
                                    <textarea class="form-control " id="option_{{$key}}" name="option[{{$key}}]" required>{{$option->option}}</textarea>
                                        
                                </div>
                            </div>
                            @endforeach

                        <div class="form-actions">
                            <button type="submit" class="btn green submit">Save</button>
                        </div>
                    </form>

                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>

    </div>

@endsection
<?php
    $js_data = array();
?>
@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
@endsection

@section('page-level-scripts-js')
    <script src="{{ URL::asset('template/assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/pages/scripts/components-bootstrap-select.min.js') }}" type="text/javascript"></script>

    @if(Session::has('status'))
        <script>alert('{{Session::get('status')}}');</script>
    @endif
@endsection