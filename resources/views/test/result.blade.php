<!DOCTYPE html>
<html lang="en">
<head>
  <title>Mcqtest </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="{{ URL::asset('template/assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('template/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
  <link href="{{ URL::asset('template/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  
    
</head>
<body>
<div class="" >
    <nav class="navbar navbar-default" role="navigation">
        <div class="">
        
        <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-brand-centered">
                @if(isset($test))
                <ul class="nav navbar-nav">
                    <li>
                        <h3><b>Online MCQTEST Result</b></h3>
                    </li>
                </ul>
                @endif
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <p style="margin:0px"><b><h4>Name: </b> {{$test->name}} </h4></p>
                <p style="margin:0px"><b><h4>Email: </b> {{$test->email}} </h4></p>
                <p style="margin:0px"><b><h4>Total Questions: </b> {{$test->total_questions}} </h4></p>
                <p style="margin:0px"><b><h4>Correct Answers: </b> {{$test->right_answers}} </h4></p>
                <p style="margin:0px"><b><h4>Incorrect Answers: </b> {{$test->wrong_answers}} </h4></p>
                <p style="margin:0px"><b><h4>Result: </b> {{$test->right_answers}} / {{$test->total_questions}} </h4></p>
            </b>
        </div>
    </div>
    <div class="row" style="margin-top: 50px; padding: 10px">
        
        <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-4" style="margin-top: 10px">
            <center>
                <a href="{{URL('/')}}"><label class="btn btn-success" id="final_submit" style="padding: 20px">Back To Home</label></a>
            </center>
        </div>
    </div>

</div>


</body>
</html>


