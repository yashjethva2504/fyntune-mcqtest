<!DOCTYPE html>
<html lang="en">
<head>
  <title>Mcqtest </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="{{ URL::asset('template/assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('template/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
  <link href="{{ URL::asset('template/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <style type="text/css">
    .overlay {display : block;position : fixed;z-index: 100;background: url('{{URL('/loader.gif')}}') center center no-repeat;background-color:#fff;opacity : 0.6;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;}
    </style>
  
    <style type="text/css">
        #question span, p {
            font-size: 18px!important;
            line-height: 24px!important;
            font-family: "Times New Roman", Times, serif!important;
        }  
        .options_opt span, p {
            font-size: 16px!important;
            margin-left: 50px;
            line-height: 20px!important;
            font-family: "Times New Roman", Times, serif!important; 
            font-weight: normal!important;
        }
    </style>
</head>
<body>

<div class="row" >
    <div class="overlay" id="overlay">
        <div id="loading-img"></div>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12" >
        <center>
            <table class="table" style="max-width: 100%">
                <tr>
                    <td><b>Name : </b></td>
                    <td><span>{{$student['name']}}</span></td>
                </tr>
                <tr>
                    <td><b>Email : </b></td>
                    <td><span >{{$student['email']}}</span></td>
                </tr>
            </table>
        </center>
    </div>
</div>

<div class="yuva-box">
    <div class="row" style="padding: 10px">
        
        <div class="col-sm-12 col-md-12" style="border: 1px solid #bfbfbf; padding: 14px">
            
            <div class="question-box" style="border: 1px solid #bfbfbf; padding: 15px">
                <div class="row ">
                    <div class="col-md-12 question_row">
                        <h3>
                            <span id="category"></span>
                        </h3>
                        <h3>
                            <span id="question"></span>
                        </h3>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <hr>
                        <label class="radio-container" style="font-weight: normal;">
                            <input type="radio" name="selected_option" data-seq = "a" class="option_check" style="margin-right:10px!important">
                            <span class="option_a options_opt" id="option_0"></span>

                            <span class="checkmark check_a"></span>
                        </label>
                    </div>
                </div>
                
                <div class="row">
                    
                    <div class="col-md-12">
                        <hr>
                        <label class="radio-container" style="font-weight: normal;">
                            <input type="radio" name="selected_option" data-seq = "b" class="option_check" style="margin-right:10px!important">
                            <span class="option_b options_opt" id="option_1"></span>
                            <span class="checkmark check_b"></span>
                        </label>
                    </div>
                </div>
                
                <div class="row">
                    
                    <div class="col-md-12">
                        <hr>
                        <label class="radio-container" style="font-weight: normal;">
                            <input type="radio" name="selected_option" data-seq = "c" class="option_check" style="margin-right:10px!important">
                            <span class="option_c options_opt" id="option_2"></span>
                            <span class="checkmark check_c"></span>
                        </label>
                    </div>
                </div>
                
                <div class="row">
                    
                    <div class="col-md-12">
                        <hr>
                        <label class="radio-container" style="font-weight: normal;">
                            <input type="radio" name="selected_option" data-seq = "d" class="option_check" style="margin-right:10px!important">
                            <span class="option_d options_opt" id="option_3"></span>
                            <span class="checkmark check_d"></span>
                        </label>
                    </div>
                </div>
                
                <div class="row">
                    <hr>
                    <div class="col-md-3">
                        <label class="btn btn-danger" id="reset_selection">Reset Selection</label>
                    </div>
                </div>
            </div>

            <div class="yuva-box" style="padding: 10px; margin-top: 10px">
                <label class="btn pull-left btn-info" id="prev">Previous</label>
                <label class="btn pull-right btn-info" id="next">Next</label>
            </div>
            
            <div class="row" style="margin-top: 20px; padding: 10px">
                <div class="col-md-4 col-sm-12 col-xs-12" style="margin-top: 10px">
                    <center>
                        <table class="table" style="max-width: 100px">
                            <tr>
                                <td>Unanswered</td>
                                <td><span id="unanswered"></span></td>
                            </tr>
                            <tr>
                                <td>Answered</td>
                                <td><span id="answered"></span></td>
                            </tr>
                        </table>
                    </center>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12" style="margin-top: 10px">
                    <center>
                        <label class="btn btn-success" id="final_submit" style="padding: 20px">Final Submit</label>
                        <label class="btn btn-danger hide" id="final_confirm" style="padding: 20px">Confirm Submit</label>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>

<script type="text/javascript">
    var questions = <?php echo json_encode($questions); ?>;
    var options = <?php echo json_encode($options); ?>;
    var student = <?php echo json_encode($student); ?>;
    var currentQuestion = 1;
    var userAnswers = [];
    var selectedOption = "";
    var currentQuestionId = 0;
    var answered = 0;
    var unanswered = questions.length;
    var rightAnswers = 0;
</script>
<script type="text/javascript">
    
    $( document ).ready(function() {
        
        $('#overlay').hide();
        fetchCurrentQuestion();
        $('#unanswered').html(unanswered);
        $('#answered').html(answered);

    });
    function fetchCurrentQuestion()
    {
        calculateAnsweredCount();        
        $('.option_check').prop("checked", false);
        if(currentQuestion == 1)
        {
            $("#prev").hide();
        }
        else{
            $("#prev").show();   
        }

        if(currentQuestion == 10)
        {
            $("#next").hide();
        }
        else{
            $("#next").show();   
        }

        $('#question').html("<p>"+questions[currentQuestion - 1]['question']+"</p><input type='hidden' value='"+questions[currentQuestion - 1]['id']+"' class='question"+questions[currentQuestion - 1]['id']+"'>");

        currentQuestionId = questions[currentQuestion - 1]['id'];

        $('#category').html("<p><h4><b>Category : </b>"+questions[currentQuestion - 1]['category']['name']+"</h4></p>");
            
        if(userAnswers.length > 0)
        {
            userAnswers.map((selectedAnswer) => {
            
                if(selectedAnswer['question_id'] == questions[currentQuestion - 1]['id'])
                {    

                    $.each( questions[currentQuestion - 1]['options'], function( index, value ){
                        
                        if(questions[currentQuestion - 1]['type']['name'] == "boolean")
                        {
                            
                            if(index < 2)
                            {
                                if(selectedAnswer['selected_option'] == value['id'])
                                {
                                    $("#option_"+index).prev().prop('checked',true);
                                }
                                
                                $("#option_"+index).html(value['option']+"<input type='hidden' value='"+value['id']+"' question-value='"+questions[currentQuestion - 1]['id']+"' class='optionValue option"+value['id']+"'>"); 
                                $('#option_2').parent().parent().parent().hide();
                                $('#option_3').parent().parent().parent().hide();

                            }
                            
                        }
                        else
                        {

                            
                            if(selectedAnswer['selected_option'] == value['id'])
                            {
                                $("#option_"+index).prev().prop('checked',true);
                            }
                            $('#option_'+index).parent().parent().parent().show();
                            $("#option_"+index).html(value['option']+"<input type='hidden' value='"+value['id']+"' question-value='"+questions[currentQuestion - 1]['id']+"' class='optionValue option"+value['id']+"'>");    
                        }
                        
                        
                    });
                }
                else
                {
                    $.each( questions[currentQuestion - 1]['options'], function( index, value ){
                    
                        if(questions[currentQuestion - 1]['type']['name'] == "boolean")
                        {
                            
                            if(index < 2)
                            {
                                $("#option_"+index).html(value['option']+"<input type='hidden' value='"+value['id']+"' question-value='"+questions[currentQuestion - 1]['id']+"' class='optionValue option"+value['id']+"'>");    
                                $('#option_2').parent().parent().parent().hide();
                                $('#option_3').parent().parent().parent().hide();

                            }
                            
                        }
                        else
                        {
                            $('#option_'+index).parent().parent().parent().show();
                            $("#option_"+index).html(value['option']+"<input type='hidden' value='"+value['id']+"' question-value='"+questions[currentQuestion - 1]['id']+"' class='optionValue option"+value['id']+"'>");    
                        }
                        
                        
                    });
                }
            });
        }
        else{



            
            
            $.each( questions[currentQuestion - 1]['options'], function( index, value ){
                
                if(questions[currentQuestion - 1]['type']['name'] == "boolean")
                {
                    
                    if(index < 2)
                    {
                        $("#option_"+index).html(value['option']+"<input type='hidden' value='"+value['id']+"' question-value='"+questions[currentQuestion - 1]['id']+"' class='optionValue option"+value['id']+"'>");    
                        $('#option_2').parent().parent().parent().hide();
                        $('#option_3').parent().parent().parent().hide();

                    }
                    
                }
                else
                {
                    $('#option_'+index).parent().parent().parent().show();
                    $("#option_"+index).html(value['option']+"<input type='hidden' value='"+value['id']+"' question-value='"+questions[currentQuestion - 1]['id']+"' class='optionValue option"+value['id']+"'>");    
                }
            });
        }
    }

    $(document).on('click','#next',function(){
        currentQuestion = currentQuestion + 1;
        
        fetchCurrentQuestion();
    })

    $(document).on('click','#prev',function(){
        currentQuestion = currentQuestion - 1;
        storeAnswer();
        fetchCurrentQuestion();
    })

    $(document).on('change', '.option_check', function(){
        var optionId = $(this).parent().find('.optionValue').val();
        var questionId = $(this).parent().find('.optionValue').attr('question-value');
        currentQuestionId = questionId;
        
        storeAnswer(optionId,questionId);
    });
    function storeAnswer(optionId,questionId)
    {
        var isQuestionAvailable = 0;

        if(userAnswers.length > 0)
        {
            userAnswers.map((selectedAnswer) => {
                if(selectedAnswer['question_id'] == questionId)
                {
                    isQuestionAvailable = 1;   
                    selectedAnswer['selected_option'] = optionId;
                    options.map((optionData) => {
                        
                        if(optionData.id == optionId)
                        {
                            if(optionData.is_correct == 1)
                            {
                                selectedAnswer['is_correct'] = 1;
                                
                            }
                            else{
                                selectedAnswer['is_correct'] = 0;
                                
                            }
                        }
                    });
                }
            });
            if(isQuestionAvailable == 0)
            {

                var temp = [];
                temp['selected_option'] = optionId;
                temp['question_id'] = questionId;

                options.map((optionData) => {
                        
                    if(optionData.id == optionId)
                    {
                        if(optionData.is_correct == 1)
                        {
                            temp['is_correct'] = 1;
                            userAnswers.push(temp);
                            
                        }
                        else{
                            temp['is_correct'] = 0;
                            userAnswers.push(temp);
                            
                        }
                    }
                });
            }
        }
        else
        {
            var temp = [];
            temp['selected_option'] = optionId;
            temp['question_id'] = questionId;

            options.map((optionData) => {
                    
                if(optionData.id == optionId)
                {
                    if(optionData.is_correct == 1)
                    {
                        temp['is_correct'] = 1;
                        userAnswers.push(temp);
                        
                    }
                    else{
                        temp['is_correct'] = 0;
                        userAnswers.push(temp);
                        
                    }
                }
            });
        }
        setTimeout(function(){
            calculateAnsweredCount();
            console.log(userAnswers);
        }, 1000);
        

    }

    function calculateAnsweredCount(){
        answered = 0;
        rightAnswers = 0;
        unanswered = questions.length;
        userAnswers.map((selectedAnswer) => {
            if(selectedAnswer['selected_option'] != undefined)
            {
                answered = answered + 1;
                unanswered = unanswered - 1;
                if(selectedAnswer['is_correct'] == 1){
                    rightAnswers = rightAnswers + 1;
                }
            }

        });

        $('#unanswered').html(unanswered);
        $('#answered').html(answered);
    }

    $(document).on('click','#reset_selection',function(){
        $('.option_check').prop("checked", false);
        storeAnswer(undefined,currentQuestionId);
    });

    $(document).on('click', '#final_submit', function(){

        

        if(confirm("Are you sure you want to submit all the answers?"))
        {
            storeDataInDB();
            $('#overlay').show();
            
        }
        else
            return false;
    });

    function storeDataInDB(){
        datasets = {
            student_name: student['name'],
            student_email: student['email'],
            total_questions: questions.length,
            right_answers: rightAnswers,
            wrong_answers: questions.length - rightAnswers
        };
        setTimeout(function(){
            $.ajax({
                url: "{{URL('submit-student-response')}}",
                enctype: 'multipart/form-data',
                type: "POST",
                data: datasets,
                success: function(res){
                    var url = '{{URL("/result")}}/'+res;
                    window.location.replace(url);
                    $('#overlay').hide();
                    
                }, error: function(res){
                    alert('Something Went Wrong. ');
                    $('#overlay').hide();
                }
            });
        }, 3000);
    }


</script>
