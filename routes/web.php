<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});
Route::get('/admin', function () {
    return view('welcome');
});
Route::get('/home', function () {
    return view('welcome');
});

Route::get('/test-login', function () {
    return view('test.test_main');
});

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout', function () {
    return abort(404);
});

Route::post('test','TestController@login');
Route::post('submit-student-response','TestController@store');

Route::get('result/{id}','TestController@result');


Route::group(['middleware' => ['web','auth']], function () {

   	
    Route::resource('admin/question-master', 'QuestionMasterController');

    Route::get('admin/result', 'TestController@results');
    Route::get('admin/user-result/{id}', 'TestController@userResults');

});

